package info.androidhive.retrofit.rest;

import info.androidhive.retrofit.model.BillResponse;
import info.androidhive.retrofit.model.ItemDetail;
import info.androidhive.retrofit.model.ItemResponse;
import info.androidhive.retrofit.model.Location;
import info.androidhive.retrofit.model.Message;
import info.androidhive.retrofit.model.Order;
import info.androidhive.retrofit.model.RecipeResponse;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface ApiInterface {
    @GET("recipes")
    Call<RecipeResponse> getRecipes();

    @GET("getitems")
    Call<ItemResponse> getRecipeItems(@Query("recipe_id") String recipeName);

    @GET("getitem")
    Call<ItemDetail> getItemDetails(@Query("item_id") String itemID);

    @GET("login")
    Call<Message> login(@Query("u_id") String uid, @Query("pass") String pass, @Query("cart") String cart);

    @GET("register")
    Call<Message> register(@Query("u_id") String uid, @Query("pass") String pass);

    @GET("item_location")
    Call<Location> getLocation(@Query("location") int itemId);

    @GET("add_item")
    Call<Message> addItem(@Query("item_id") String itemID, @Query("cart_id") String cartID, @Query("qty") int qty );

    @GET("checkout")
    Call<BillResponse> checkout(@Query("cart_id") String cartID);

    @GET("order")
    Call<Order> order(@Query("user_id") String userID, @Query("cart_id") String cartID);

    @GET("payment")
    Call<Message> payment(@Query("order_id") String orderID);
}
