package info.androidhive.retrofit.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.List;

import info.androidhive.retrofit.R;
import info.androidhive.retrofit.adapter.BillAdapter;
import info.androidhive.retrofit.adapter.ItemAdapter;
import info.androidhive.retrofit.adapter.RecipeAdapter;
import info.androidhive.retrofit.model.BillItem;
import info.androidhive.retrofit.model.BillResponse;
import info.androidhive.retrofit.model.Item;
import info.androidhive.retrofit.model.ItemResponse;
import info.androidhive.retrofit.model.Message;
import info.androidhive.retrofit.model.Order;
import info.androidhive.retrofit.model.Recipe;
import info.androidhive.retrofit.model.RecipeResponse;
import info.androidhive.retrofit.rest.ApiClient;
import info.androidhive.retrofit.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static info.androidhive.retrofit.activity.LoginActivity.CART;
import static info.androidhive.retrofit.activity.LoginActivity.USER;

public class CheckoutActivity extends AppCompatActivity {
    SharedPreferences mSettings;
    TextView txt;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        mSettings = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        final String cartID = mSettings.getString(CART, null);
        final String userID = mSettings.getString(USER, null);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.payment_fab);
        txt = (TextView) findViewById(R.id.total_bill);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<Order> call = apiService.order(userID, cartID);
            call.enqueue(new Callback<Order>() {
                @Override
                public void onResponse(Call<Order> call, Response<Order> response) {
                    Order s = response.body();
                    if(s.getMessage().equals("order placed")) {
                        Intent intent= new Intent(CheckoutActivity.this, PaymentActivity.class);
                        intent.putExtra("order_id", s.getId().toString());
                        startActivity(intent);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), s.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Order> call, Throwable t) {
                    Log.e("Hello", t.toString());
                }
            });
            }
        });

        final RecyclerView recyclerView = (RecyclerView) findViewById(R.id.bill_item_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<BillResponse> call = apiService.checkout(cartID);
        call.enqueue(new Callback<BillResponse>() {
            @Override
            public void onResponse(Call<BillResponse> call, Response<BillResponse> response) {
                int statusCode = response.code();
                List<BillItem> items = response.body().getItems();
                Log.i("items", items.toString());
                BillAdapter mAdapter =new BillAdapter(items, R.layout.bill_item_row, getApplicationContext());
                recyclerView.setAdapter(mAdapter);
                txt.setText("Total: " + response.body().getTotal());
                mAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<BillResponse> call, Throwable t) {
                Log.e("Hello", t.toString());
            }
        });
    }
}
