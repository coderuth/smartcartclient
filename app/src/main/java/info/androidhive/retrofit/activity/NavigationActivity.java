package info.androidhive.retrofit.activity;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import info.androidhive.retrofit.model.Location;
import info.androidhive.retrofit.model.Message;
import info.androidhive.retrofit.rest.ApiClient;
import info.androidhive.retrofit.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import android.content.SharedPreferences;

import info.androidhive.retrofit.R;

import static info.androidhive.retrofit.activity.LoginActivity.CART;

public class NavigationActivity extends AppCompatActivity implements SensorEventListener {

    //Step Counter Stuff
    SensorManager sensorManager;
    Sensor aSensor;
    boolean flag = true;
    float steps;
    float temp;
    int temp2;
    String itemID;

    //Compass Stuff
    float[] mGravity;
    float[] mGeomagnetic;
    float azimut = 0f;

    SensorManager mSensorManager;
    Sensor mSensor;

    ImageView arrow;
    ImageView DCEFloor;
    private float currentDegree = 0f;
    int self = Animation.RELATIVE_TO_SELF;
    int y;
    int x;

    String currentLocation;
    String destinationLocation;
    ImageView destinationMarker;
    SharedPreferences mSettings;
    Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        Toast.makeText(getApplicationContext(), "You can Fine Tune your location by touching or draging the pointer to your precise location" ,
                Toast.LENGTH_LONG).show();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSettings = this.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        arrow = (ImageView) findViewById(R.id.imageView);

        DCEFloor = (ImageView) findViewById(R.id.imageView2);

        bundle = getIntent().getExtras();

        destinationMarker = (ImageView) findViewById(R.id.imageView3);
        DCEFloor.setImageResource(R.drawable.bottom_final);

        int itemID = bundle.getInt("item_id");
        setDestinationMarker(itemID);
        setCurrentMarker();

        //final TextView coor = (TextView) findViewById(textView6);
        final View touchView = findViewById(R.id.touchID);
        touchView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            int y = Math.round(event.getY()) - arrow.getTop();
            arrow.setTop(Math.round(event.getY()));
            arrow.setBottom(arrow.getBottom() + y);

            int x = Math.round(event.getX()) - arrow.getLeft();
            arrow.setLeft(Math.round(event.getX()));
            arrow.setRight(arrow.getRight() + x);
            return true;
            }
        });
    }

    public void setDestinationMarker(int itemID) {
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Log.i("itemid", Integer.valueOf(itemID).toString());
        Call<Location> call = apiService.getLocation(itemID);
        call.enqueue(new Callback<Location>() {
            @Override
            public void onResponse(Call<Location> call, Response<Location> response) {
                Location s = response.body();
                destinationMarker.setY(s.getY());
                destinationMarker.setX(s.getX());
            }

            @Override
            public void onFailure(Call<Location> call, Throwable t) {
                Log.e("Hello", t.toString());
            }
        });

    }

    public void setCurrentMarker() {
        final View view = findViewById(R.id.touchID);
        view.getViewTreeObserver().addOnGlobalLayoutListener(
            new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    int y = 640 - arrow.getTop();
                    arrow.setTop(640);
                    arrow.setBottom(arrow.getBottom() + y);

                    int x = 450 - arrow.getLeft();
                    arrow.setLeft(450);
                    arrow.setRight(arrow.getRight() + x);
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
    }

    @Override
    protected void onResume() {
        super.onResume();
        aSensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        if (aSensor != null) {
            sensorManager.registerListener(this, aSensor, SensorManager.SENSOR_DELAY_UI);
        } else {
            Toast.makeText(getApplicationContext(), "No Step Counter Sensor Detected", Toast.LENGTH_LONG).show();
        }

        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        if (mSensor != null) {
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
        } else {
            Toast.makeText(getApplicationContext(), "No Orientation Sensor Detected", Toast.LENGTH_LONG).show();
        }

        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        if (mSensor != null) {
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME);
        } else {
            Toast.makeText(getApplicationContext(), "No Accelerometer Sensor Detected", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_STEP_COUNTER) {
            if (flag == true) {
                steps = sensorEvent.values[0];
                flag = false;
            }
            temp = sensorEvent.values[0] - steps;
            temp2 = Math.round(temp);
            double var = Math.cos(Math.toRadians(currentDegree));
            var = var*14.293;
            float var2 = (float) var;
            double var3 = Math.sin(Math.toRadians(currentDegree));
            var3 = var3 * 14.293;
            float var4 = (float) var3;

            int var5 = Math.round(var2);
            int var6 = Math.round(var4);
            arrow.setTop(arrow.getTop() -  var5);
            arrow.setBottom(arrow.getBottom() - var5);
            arrow.setRight(arrow.getRight() +  var6);
            arrow.setLeft(arrow.getLeft() + var6);
            //reading.setText(currentLocation + " " + destinationLocation);
        }

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = sensorEvent.values;
        if (sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = sensorEvent.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];

            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];

                SensorManager.getOrientation(R, orientation);
                azimut = orientation[0];
            }

            azimut = azimut * 60;
            azimut = (float) (int) azimut;
            if (flag == true) {
                x = self;
                y = self;
                flag = false;
            }

            RotateAnimation ra = new RotateAnimation(currentDegree, azimut, self, 0.5f,
                    self, 0.5f);
            ra.setDuration(1000);
            ra.setFillAfter(true);
            arrow.startAnimation(ra);
            currentDegree = azimut;
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.bar_scan:
                Intent aboutIntent = new Intent(NavigationActivity.this, BarCodeActivity.class);
                startActivityForResult(aboutIntent, 1111);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bar, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == 1111) {
            if (data.hasExtra("item")) {
                itemID = data.getExtras().getString("item");
                ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
                String cartID = mSettings.getString(CART, null);
                Call<Message> call = apiService.addItem(itemID, cartID, bundle.getInt("qty"));
                call.enqueue(new Callback<Message>() {
                    @Override
                    public void onResponse(Call<Message> call, Response<Message> response) {
                        Message s = response.body();
                        if(s.getMessage().equals("added item to cart")) {
                            Toast.makeText(getApplicationContext(), "Added to Cart", Toast.LENGTH_LONG).show();
                            finish();
                        }
                        else{
                            Toast.makeText(getApplicationContext(), s.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Message> call, Throwable t) {
                        Log.e("Hello", t.toString());
                    }

            });
        }
    }

}
}


