package info.androidhive.retrofit.activity;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import info.androidhive.retrofit.R;
import info.androidhive.retrofit.model.ItemDetail;
import info.androidhive.retrofit.model.Message;
import info.androidhive.retrofit.model.Order;
import info.androidhive.retrofit.rest.ApiClient;
import info.androidhive.retrofit.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentActivity extends AppCompatActivity {

    Button pay;
    TextView payText;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Bundle extras = getIntent().getExtras();

        final String orderID = extras.getString("order_id");

        pay = (Button) findViewById(R.id.pay_final);
        payText = (TextView) findViewById(R.id.pay_text);

        payText.setText("Order " + orderID +" Placed, Pay at the Counter, Happy Shopping");

        pay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<Message> call = apiService.payment(orderID);
            call.enqueue(new Callback<Message>() {
                @Override
                public void onResponse(Call<Message> call, Response<Message> response) {
                    Message s = response.body();
                    if(s.getMessage().equals("paid for the order")) {
                        Intent i = new Intent(PaymentActivity.this, LoginActivity.class);
                        startActivity(i);
                    }
                    else{
                        Toast.makeText(getApplicationContext(), s.getMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<Message> call, Throwable t) {
                    Log.e("Hello", t.toString());
                }
            });

            }
        });
    }
}

