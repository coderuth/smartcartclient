package info.androidhive.retrofit.activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Toast;

import info.androidhive.retrofit.R;
import info.androidhive.retrofit.model.Recipe;

public class people extends AppCompatActivity {
    Recipe mItem;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people);
        final NumberPicker n = (NumberPicker) findViewById(R.id.num);
        Button b = (Button) findViewById(R.id.btn);
        n.setMinValue(1);
        n.setMaxValue(10);
        final Bundle extra = getIntent().getExtras();
        n.setOnValueChangedListener(onValueChangeListener);
        assert b != null;
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(people.this, RecipeItemsActivity.class);
                i.putExtra("people", n.getValue());
                i.putExtra("recipe_id", extra.getInt("recipe_id"));
                startActivityForResult(i, 777);
            }
        });
     }
    NumberPicker.OnValueChangeListener onValueChangeListener =
            new 	NumberPicker.OnValueChangeListener(){
                @Override
                public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                    Toast.makeText(people.this,
                            "selected number "+numberPicker.getValue(), Toast.LENGTH_SHORT);
                }
            };
}
