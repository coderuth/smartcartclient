package info.androidhive.retrofit.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;
import info.androidhive.retrofit.R;
import info.androidhive.retrofit.model.BillItem;
import info.androidhive.retrofit.model.Item;
import info.androidhive.retrofit.model.ItemDetail;
import info.androidhive.retrofit.activity.NavigationActivity;
import info.androidhive.retrofit.rest.ApiClient;
import info.androidhive.retrofit.rest.ApiInterface;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillAdapter extends RecyclerView.Adapter<BillAdapter.MovieViewHolder> {

    private List<BillItem> items;
    private int rowLayout;
    private final Context context;

    public class MovieViewHolder extends RecyclerView.ViewHolder {
        LinearLayout itemsLayout;
        BillItem mItem;
        TextView id, quantity, name, price;

        public MovieViewHolder(View v) {
            super(v);
            itemsLayout = (LinearLayout) v.findViewById(R.id.bill_item_layout);
            id = (TextView) v.findViewById(R.id.bill_item_id);
            quantity = (TextView) v.findViewById(R.id.bill_quantity);
            name = (TextView) v.findViewById(R.id.bill_name);
            price = (TextView) v.findViewById(R.id.bill_price);
        }

        public void setItem(final BillItem item) {
            mItem = item;
            // Make another request to get Item
            id.setText(item.getItemId().toString());
            name.setText(item.getName().toString());
            price.setText(item.getPrice().toString());
            quantity.setText(item.getQuantity().toString());
        }
    }

    public BillAdapter(List<BillItem> items, int rowLayout, Context context) {
        this.items = items;
        this.rowLayout = rowLayout;
        this.context = context;
    }

    @Override
    public BillAdapter.MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(rowLayout, parent, false);
        return new MovieViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {
        holder.setItem(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}